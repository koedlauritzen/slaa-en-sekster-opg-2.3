﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolUI
{
    public class Terning
    {
        private int _antalSider { get; set; }
        private Random tilfaeldig = new Random();

        public int TerningOeje = 1;
        public Terning(int AntalSider)
        {
            _antalSider = AntalSider;
        }

        public void KastTerning()
        {   
            TerningOeje = tilfaeldig.Next(1, _antalSider + 1);
        }
    }
}

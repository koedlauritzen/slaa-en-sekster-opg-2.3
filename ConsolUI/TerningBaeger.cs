﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolUI
{
    public class TerningBaeger
    {
        private int _antalTerninger { get; set; }
        private int _antalTerningSider { get; set; }
        public List<Terning> Terninger = new List<Terning>();
        public TerningBaeger(int AntalTerninger, int AntalTerningSider)
        {
            _antalTerninger = AntalTerninger;
            _antalTerningSider = AntalTerningSider;

            for (int i = 0; i < _antalTerninger; i++)
            {                
                Terninger.Add(new Terning(_antalTerningSider));
            }
        }

        public void RystBaeger()
        {
            foreach (var terning in Terninger)
            {
                terning.KastTerning();
            }
        }

        public void LoeftBaeger()
        {
            for (int i = 0; i < Terninger.Count; i++)
            {
                Console.WriteLine($"Terning { i + 1 }: {Terninger[i].TerningOeje}");
            }
        }
    }
}
